# Disallow all unpatched versions of libtorrent
net-libs/libtorrent::gentoo
>=net-p2p/rtorrent-0.9.2

# Not really stable
x11-terms/st::local_overlay

# Desperately needs updated dependencies
dev-haskell/diagrams-cairo::local_overlay
dev-haskell/diagrams-contrib::local_overlay
dev-haskell/diagrams-core::local_overlay
dev-haskell/diagrams-gtk::local_overlay
dev-haskell/diagrams-lib::local_overlay
dev-haskell/diagrams-svg::local_overlay
dev-haskell/netwire::local_overlay

# Unwanted “features”
>=www-client/firefox-26

# Bullshit
>=media-sound/pulseaudio-4.99
# and the stuff that requires it
>media-plugins/alsa-plugins-1.0.27-r1
>media-libs/libsdl-1.2.15-r4
>media-libs/sdl-image-1.2.12
>media-libs/sdl-mixer-1.2.12-r3

# Changed header layout breaks tons of programs
>=media-libs/freetype-2.5

# We want man-db instead
sys-apps/man

# I can't figure out why this is a dependency in the first place
#sys-auth/consolekit

# Potentially a cause of mplayer2 white line issue, debugging
media-libs/libass::local_overlay
# media-libs/harfbuzz::local_overlay

# Let's avoid things we don't necessarily need
*/*::sunrise
*/*::powerman
*/*::mpd
*/*::multimedia
*/*::miramir
*/*::x11
*/*::gamerlay
*/*::maggu2810-overlay
*/*::booboo
*/*::emery
*/*::ixit

# No longer needed
#x11-misc/xmobar::local_overlay
#media-video/mpv::local_overlay
app-admin/pass::local_overlay

# Portage can't resolve these slot conflicts, help out a bit
<dev-haskell/attoparsec-0.11
<dev-haskell/aeson-0.7

# Dependency issues
<dev-haskell/text-1

# Skipping this version
# =dev-lang/ghc-7.6.3*

# Background color issues
>=media-gfx/sxiv-1.2
