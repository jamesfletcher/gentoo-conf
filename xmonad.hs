import Control.Monad (void)

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageHelpers
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.WorkspaceCompare
import XMonad.Actions.CycleWS
import XMonad.Actions.NoBorders
import XMonad.Layout.NoBorders

import XMonad.Hooks.EwmhDesktops

import qualified XMonad.StackSet as W

import System.IO
import System.Posix.Files (touchFile)

import Data.Maybe (fromJust, maybeToList)
import qualified Data.Map as M (toList)
import Data.Ord (comparing)
import qualified Data.List as L

import Control.Monad.Error.Class (MonadError)
import qualified Network.MPD as MPD
import qualified Network.MPD.Commands.Extensions as MPD

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad $ withUrgencyHook (NoUrgencyHook)
           $ defaultConfig {
        manageHook          = manageFloats <+> manageDocks <+> manageHook defaultConfig,
        layoutHook          = smartBorders $ avoidStruts $ layoutHook defaultConfig,
        handleEventHook     = handleEventHook defaultConfig <+> docksEventHook <+> fullscreenEventHook,
        logHook             = dynamicLogWithPP xmobarPP {
            ppOutput  = hPutStrLn xmproc,
            ppTitle   = xmobarColor "white" "",
            ppCurrent = xmobarColor "#85c600" ""
        },

        modMask             = mod4Mask, -- Capslock

        terminal            = "urxvtc",

        normalBorderColor   = "#0e1112",
        focusedBorderColor  = "#85919b",

        focusFollowsMouse   = False,
        clickJustFocuses    = False,

        workspaces = workspaceNames 9

    } `additionalKeys` extraKeys


extraKeys =
  [ ((0, xK_Print), spawn "scrot -e 'optipng $f; mv $f ~/scrot/'")

  , ((mod4Mask, xK_h), windows W.focusMaster)
  , ((mod4Mask, xK_l), windows moveRight)

  , ((mod4Mask .|. shiftMask, xK_h), sendMessage Shrink)
  , ((mod4Mask .|. shiftMask, xK_l), sendMessage Expand)

  , ((mod4Mask, xK_Up), windows moveUp)
  , ((mod4Mask, xK_Down), windows moveDown)
  , ((mod4Mask, xK_Left), windows W.focusMaster)
  , ((mod4Mask, xK_Right), windows moveRight)

  , ((mod4Mask, xK_Tab), nextWS')
  , ((mod4Mask .|. shiftMask, xK_Tab), prevWS')

    -- never terminate X please
  , ((mod4Mask .|. shiftMask, xK_q), return ())

  , ((mod4Mask, xK_r), spawn "$(yeganesh -x -- -fn -*-terminus-medium-r-normal-*-14-*-*-*-*-*-*-* -i -nf '#daccbb' -nb '#0e1112')")

    -- lock the screen when not in use
  , ((mod4Mask, xK_s), spawn "slock")

    -- reset the mouse cursor
  , ((mod4Mask, xK_Escape), spawn "swarp 0 0")

  , ((controlMask .|. mod1Mask, xK_Home),      io . void $ MPD.withMPD MPD.toggle)
  , ((controlMask .|. mod1Mask, xK_Insert),    io . void $ MPD.withMPD (MPD.play Nothing))
  , ((controlMask .|. mod1Mask, xK_End),       io . void $ MPD.withMPD MPD.stop)
  , ((controlMask .|. mod1Mask, xK_Page_Down), io . void $ MPD.withMPD MPD.next)
  , ((controlMask .|. mod1Mask, xK_Page_Up),   io . void $ MPD.withMPD MPD.previous)
  ]
  -- Switch workspaces using symbols
  ++ [ ((mod4Mask .|. m, k), windows $ f i)
     | (i, k) <- zip (workspaceNames 9)
       [ xK_exclam, xK_at, xK_numbersign, xK_dollar, xK_percent, xK_asciicircum
       , xK_ampersand, xK_asterisk, xK_bracketleft ]
     , (m, f) <- [(0, W.greedyView), (shiftMask, W.shift)]
     ]

friendlyNames = [
        (1, "term"),
        (2, "editor"),
        (3, "irc"),

        (7, "skype"),
        (8, "torrent"),
        (9, "web")
    ]

workspaceNames n = map elem [1..n]
  where
    elem x = show x ++ (concatMap (':':) . maybeToList $ lookup x friendlyNames)

-- Float exceptions
manageFloats = composeAll $ mpvF : [ title =? x `to` doFloat | x <- floatTitles ]
    where to = (-->)

floatTitles =
  [ "Firefox Preferences", "About Firefox", "Resize Canvas"
  , "Downloads", "Software Update", "World of Warcraft", "Limbo"]

mpvF = (L.isPrefixOf "mpv" `fmap` title) --> doFullFloat

-- Cycle focus inside the current stack
moveUp = W.modify' moveUp'
moveUp' s@(W.Stack _ [] _)          = s -- master is unchanged
moveUp' s@(W.Stack _ [_] _)         = s -- last before master is unchanged
moveUp' (W.Stack f (u:us) ds)       = W.Stack u us (f:ds) -- rest is moved

moveDown = W.modify' moveDown'
moveDown' s@(W.Stack _ [] _)        = s -- master is unchanged
moveDown' s@(W.Stack _ _ [])        = s -- bottom is unchanged
moveDown' (W.Stack f us (d:ds))     = W.Stack d (f:us) ds -- rest is moved

moveRight = W.modify' moveRight'
moveRight' (W.Stack m [] (d:ds))    = W.Stack d [m] ds -- master, move focus to top
moveRight' s@(W.Stack _ _ _)        = s -- only one window or not master

switchWorkspace' d = wsBy' d >>= windows . W.greedyView
wsBy' = findWorkspace getSortByIndex Next HiddenNonEmptyWS

nextWS' = switchWorkspace' 1
prevWS' = switchWorkspace' (-1)

{-
data AllFloat = AllFloat
  deriving (Read, Show)

instance SetsAmbiguous AllFloat where
  hiddens _ ws _ _ = map fst . M.toList $ W.floating ws
-}
