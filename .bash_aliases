# initialize tc queue control
alias sfb='sudo tc qdisc add dev eth0 root handle 1: sfb'

# grep should always be case insensitive
alias grep='grep -i --color=auto'

# some more ls aliases
alias ll='ls -l --si'
alias la='ls -A'
alias l='ls -CF'

alias df='df --total -H'
alias du='du --si'
alias ds='du --total --summarize -- *'

# start in fullscreen mode
alias sxiv='sxiv -Zf'

# quicker root delete
alias srm='sudo rm'

# prefer floating vim over gvim
alias gvim='urxvt -e vim'
alias gview='urxvt -e vim'
alias svim='sudo vim'

# new mail client and stuff
alias m='notmuch new >/dev/null && ~/dev/bower/bower'

# restore secondary monitor after fullscreen botches it
alias fixmon='xrandr --output DFP7 --mode 1920x1200 --primary --preferred --output CRT1 --mode 1280x1024 --right-of DFP7'
alias fixcol='argyll-dispwin -d2 ~/icc/altlut; argyll-dispwin -d1 -c ~/icc/main.icc'
alias fixpulse='sudo fixpulse'

# fix fanspeed
alias fanspeed='aticonfig --pplib-cmd "set fanspeed 0 18"'

# ghetto post-it notes
alias note='cat >> ~/notes'

# quad core make (ht)
alias make='make -j9'

# launch weechat inside dtach
alias weechat='dtach -A /tmp/weechat -Ez weechat-curses'

# scan for replaygain
alias replayscan='find -L . -name \*.flac -printf %h\\n | sort -u | while read -r d; do flacgain "$d"/*.flac; done'
alias mp3scan='find -L . -name \*.mp3 -printf %h\\n | sort -u | while read -r d; do mp3gain "$d"/*.mp3; done'

# automatically copy URL
alias wgetpaste='wgetpaste -X'
alias hpaste='wgetpaste -l Haskell'

# simpler filenames
alias youtube-dl='youtube-dl --id'

# automatically resume and stuff
alias wget='wget -c -4 --timeout 10'

# Got camera photos and stuff
alias photo='sudo gphoto2 --get-all-files --new && sudo chown nand:nand IMG_*.JPG'

# portage aliases + auto sudo
alias emerge='sudo emerge'
alias euse='sudo euse'
alias eselect='sudo eselect'
alias ebuild='sudo ebuild'
alias eix-update='sudo eix-update'
alias eix-sync='sudo eix-sync'
alias layman='sudo layman'
alias genlop='sudo genlop'
alias egencache='sudo egencache'
alias econf='sudo dispatch-conf'
alias eclean='sudo eclean'
alias erebuild='sudo revdep-rebuild'
alias edepclean='emerge --depclean'
alias module-rebuild='sudo module-rebuild'
alias rc-update='sudo rc-update'
alias rc-config='sudo rc-config'
alias rc-service='sudo rc-service'
alias haskell-updater='sudo haskell-updater'
alias hackport='sudo hackport -p /usr/local/portage'
alias ghc-pkg='sudo ghc-pkg'
alias elogv='sudo elogv'

alias esync='emerge --ask=n --sync'
alias epull='layman -S && hackport update && esync && eix-update'
alias eworld='emerge --update --newuse --deep @world --exclude portage'
alias deworld='emerge --ask=n --deselect'
alias esmart='sudo smart-live-rebuild -E'
alias eupdate='epull && eworld && haskell-updater && esmart; squash-update'

# squashfs maintenance aliases
alias squash-save='sudo squash-save'
alias squash-reload='for i in /etc/init.d/squash*; do sudo $i restart; done'
alias squash-update='squash-save && squash-reload'

# dual booting
alias pm-restart='sudo pm-restart'

# make sure we don't break our system
alias restart='squash-save && sudo reboot'
alias shutdown='squash-save && sudo poweroff'

# start djbdns manually
# alias djbdns='sudo sh -c "nohup /var/dnscache/run >/dev/null 2>&1 &"'

# quick ssh access to VPS
alias vps='ssh nanovps -l root'
alias uni='ssh bhd48@zeus.rz.uni-ulm.de'

# Run password-store with the primary selection as default
alias pass='PASSWORD_STORE_X_SELECTION=primary pass'

# Syncplay without GUI support
alias syncplay='/home/nand/dev/syncplay/syncplayClient.py --no-gui'

# ln with absolute paths
aln() {
  LNARGS=
  while [[ $1 == -* ]]; do
    LNARGS="$LNARGS $1"
    shift
  done

  P=$(readlink -f "$1")
  shift
  ln $LNARGS "$P" "$*"
}

# Some darcs aliases
alias da='darcs'

# Darcs colordiff
daff () {
  darcs diff $@ | colordiff
}

# Deselect a package, and remove it too
demerge() {
  deworld $@ && edepclean
}

iup() {
  #curl -F"file=@$1" http://i.intma.in/up.cgi | tee /dev/stderr | xclip
  curl -F"file=@$1" http://i.srsfckn.biz/ | tee /dev/stderr | xclip -selection clipboard
}

# GHCid integration

alias ghcid='echo'

alias :quit='exit'
# alias :='ghcid :'
alias :add='ghcid :add'
alias :show='ghcid :show'
alias :set='ghcid :set'
alias :unset='ghcid :unset'
alias :kill='ghcid :quit'
alias :start='ghcid start'
alias :help='ghcid :help'
alias :load='ghcid :load'
alias :info='ghcid :info'
alias :type='ghcid :type'
alias :kind='ghcid :kind'

# GHCid aliases

alias :q=':quit'
alias :h=':help'
alias :?=':help'
alias :l=':load'
alias :i=':info'
alias :t=':type'
alias :k=':kind'

# Newer GHCi

alias ghci-head='PATH="~/ghc-head/bin:$PATH" ghci'
